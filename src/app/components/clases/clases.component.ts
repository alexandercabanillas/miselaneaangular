import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clases',
  templateUrl: './clases.component.html',
  styles: []
})
export class ClasesComponent implements OnInit {
   alerta: string;
   cargando: boolean = false;

   propiedades: Object = {
     danger: false
   }

  constructor() {
    this.alerta = "alert-danger";
   }

  ngOnInit() {
  }

  ejecutar(){
    this.cargando = true;
    setTimeout(() => this.cargando = false, 3000 );
  }

}
