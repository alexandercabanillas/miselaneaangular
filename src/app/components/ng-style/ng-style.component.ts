import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-style',
  template: `
    <p [ngStyle]= "{'font-size': '40px'}">{{mensaje}}</p>
    <p [style.fontSize.px]= "tamano">{{mensaje}}</p>
    <button class ="btn btn-primary" (click)="tamano = tamano+5"><i class= "fa fa-plus"></i></button>
    <button class ="btn btn-primary" (click)="tamano = tamano-5"><i class= "fa fa-minus"></i></button>
  `,
  styles: []
})
export class NgStyleComponent implements OnInit {
  tamano: number;
  mensaje: string;

  constructor() {
      this.tamano = 25;
      this.mensaje = "Hola Mundo"
   }

  ngOnInit() {
  }

}
