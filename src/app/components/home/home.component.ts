import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
  <h1>Demos <small>angular</small></h1>
  <hr>
  <app-ng-style></app-ng-style>
  <hr>
  <app-css></app-css>
  <hr>
  <app-clases></app-clases>
  <hr>
  <p [appResaltado]="'orange'">Hola Directiva</p>
  <hr>
  <app-ng-switch></app-ng-switch>
  `,
  styles: []
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
