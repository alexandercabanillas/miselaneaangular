import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appResaltado]'
})
export class ResaltadoDirective {

  constructor(private elementRef: ElementRef) {
  }
  @Input ("appResaltado") nuevoColor:string;

  @HostListener('mouseenter') mouseEntro(){
   this.resaltarColor(this.nuevoColor || "yellow");
  }

  @HostListener('mouseleave') mouseSalio(){
    this.resaltarColor(null);
  }

  private resaltarColor(color:string){
    this.elementRef.nativeElement.style.backgroundColor = color;
  }

}
